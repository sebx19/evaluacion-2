package com.example.andrigo.sebaa;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class Fragment2 extends Fragment {

    EditText etMai, etFon;
    Button btSss;

    public Fragment2() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {


        View view = inflater.inflate(R.layout.fragment_frag2,container,false);

        etMai = (EditText)view.findViewById(R.id.etMail);
        etFon = (EditText)view.findViewById(R.id.etPhone);
        btSss = (Button) view.findViewById(R.id.btNextTwo);

        btSss.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String email = etMai.getText().toString();
                String telefono = etFon.getText().toString();


                if(!email.isEmpty()){
                    FragmentTransaction fr = getFragmentManager().beginTransaction();
                    fr.replace(R.id.fragment_container, new Fragment3());
                    fr.commit();



                }else{
                    Toast.makeText(getActivity(), "El email debe estar en el campo", Toast.LENGTH_SHORT).show();
                }


            }
        });





        return view;
    }

}

