package com.example.andrigo.sebaa;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class Fragment1 extends Fragment {

    Button botton1;
    EditText etNom, etEdd;

    public Fragment1() {

    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_frag1,container,false);

        etNom = (EditText)view.findViewById(R.id.etNombre);
        etEdd = (EditText)view.findViewById(R.id.etEdad);
        botton1 = (Button) view.findViewById(R.id.btNextOne);

        botton1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String name = etNom.getText().toString();
                String old =  etEdd.getText().toString();

                if(!name.isEmpty() ){

                    FragmentTransaction fr = getFragmentManager().beginTransaction();
                    fr.replace(R.id.fragment_container, new Fragment2());
                    fr.commit();


                }else{
                    Toast.makeText(getActivity(), "El nombre de usuario es obligatorio", Toast.LENGTH_SHORT).show();
                }

            }
        });





        return view;
    }





}
