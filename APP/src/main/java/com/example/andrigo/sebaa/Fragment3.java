package com.example.andrigo.sebaa;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;



public class Fragment3 extends Fragment {

    EditText etUser, etPass;
    Button btF;

    public Fragment3() {

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {




        View view = inflater.inflate(R.layout.fragment_frag3,container,false);

        etUser = (EditText)view.findViewById(R.id.etUsername);
        etPass = (EditText)view.findViewById(R.id.etPassword);
        btF = (Button)view.findViewById(R.id.btFinal);

        btF.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String user = etUser.getText().toString();
                String pass = etPass.getText().toString();

                if(!user.isEmpty() && !pass.isEmpty()){

                    Toast.makeText(getActivity(), "Contraseña : " +pass, Toast.LENGTH_SHORT).show();


                }else{

                    Toast.makeText(getActivity(), "Estos campos tienen que estar rellenados", Toast.LENGTH_SHORT).show();

                }

            }
        });



        return view;
    }





}
